package com.qytest.springcloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qytest.springcloud.entities.Payment;

/**
 * @author qy
 * @date 2022年07月08日 17:56
 */
public interface PaymentMapper extends BaseMapper<Payment> {

}
