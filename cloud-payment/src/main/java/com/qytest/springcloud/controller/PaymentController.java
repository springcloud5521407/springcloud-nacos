package com.qytest.springcloud.controller;

import cn.hutool.core.util.IdUtil;
import com.qytest.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author qy
 * @date 2022年07月8日 15:38
 */
@RestController
@RequestMapping("/payment")
@Slf4j
public class PaymentController {
    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/timeout/{id}")
    public String paymentInfoTimeOut(@PathVariable("id") Integer id) {
        String result = paymentService.paymentInfoTimeOut(id);
        log.info("========result:{}========", result);
        return result;
    }


    @GetMapping(value = "/ok/{id}")
    public String paymentCircuit(@PathVariable("id") Integer id) {
        if (id < 0) {
            throw new RuntimeException("id不能为负数");
        }
        String serial = IdUtil.simpleUUID();
        return "调用成功，线程池：" + Thread.currentThread().getName() + "访问"+serverPort+",serial=" + serial;
    }



}