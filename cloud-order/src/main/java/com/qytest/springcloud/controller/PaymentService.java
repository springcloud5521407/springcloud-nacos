package com.qytest.springcloud.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(value = "cloud-payment",fallback = PaymentFallbackService.class)
public interface PaymentService {

    @GetMapping(value = "/payment/ok/{id}")
    String paymentInfoOk(@PathVariable("id") Integer id);

    @GetMapping(value = "/payment/timeout/{id}")
    String paymentInfoTimeOut(@PathVariable("id") Integer id);
}
