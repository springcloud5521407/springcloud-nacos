package com.qytest.springcloud.controller;

import org.springframework.stereotype.Component;

@Component
public class PaymentFallbackService implements PaymentService {
    @Override
    public String paymentInfoOk(Integer id) {
        return "全局解耦降级处理PaymentFallback->paymentInfoOk！";
    }

    @Override
    public String paymentInfoTimeOut(Integer id) {
        return "全局解耦降级处理PaymentFallback->paymentInfoTimeOut！";
    }
}
